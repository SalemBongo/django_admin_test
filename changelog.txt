################################################ CHANGELOG ############################################################

v0.6b  --  02.10.2018

1. Added check user membership -> is_member()
2. Fixed thread-decorator
3. Cosmetic changes (names, spaces, etc.)
4. Fixed docstrings (PEP257)
5. Updated requirements.txt
6. Updated README.md

=======================================================================================================================

v0.5.15b  --  02.10.2018

1. Fixed thread-decorator

=======================================================================================================================

v0.5.14b  --  01.10.2018

1. Fixed __str__ of models in trading (app)

=======================================================================================================================

v0.5.13b  --  01.10.2018

1. Fixed docstrings (PEP257)
2. Removed dev-tools -> _log_cleaner.py
3. Updated requirements.txt

=======================================================================================================================

v0.5.12b  --  01.10.2018

1. Fixed thread -> PNL-method
2. Fixed __str__ of models in trading (app)
3. Fixed list_display in StrategyPortfolio

=======================================================================================================================

v0.5.11b  --  27.09.2018

1. Optimized methods -> get_readonly_fields()
2. Optimized field -> list_display in StrategyPortfolioAdmin

=======================================================================================================================

v0.5.10b  --  27.09.2018

1. Optimized permissions for Researchers and Superusers.

=======================================================================================================================

v0.5.9b  --  27.09.2018

1. Updated instructions in README.md

=======================================================================================================================

v0.5.8b  --  27.09.2018

1. Optimized permissions for Researchers -> added readonly_fields
2. Optimized models in trading (app)
3. Updated requirements.txt

=======================================================================================================================

v0.5.7b  --  27.09.2018

1. Optimized docstrings
2. Updated README.md

=======================================================================================================================

v0.5.6b  --  26.09.2018

1. Fixed PNL-method view in AdminBoard
2. Added PNL-method in thread
3. Optimized models in trading (app)
4. Optimized docstrings
5. Updated README.md

=======================================================================================================================

v0.5.5b  --  26.09.2018

1. Fixed PNL-method in trading (app)

=======================================================================================================================

v0.5.4b  --  24.09.2018

1. Fixed whole projects (PEP8)
2. Added utils -> logger, thread-decorator
3. Added signals in threads
4. Optimized imports

=======================================================================================================================

v0.5.3b  --  19.09.2018

1. Fixed names (PEP8)
2. Added .gitignore in repository
3. Updated requirements.txt

=======================================================================================================================

v0.5.2b  --  18.09.2018

1. Optimized signals in accounts (app) -> Moved to Researcher models like a methods
2. Added models BaseTicker, QuoteTicker, Exchange, Pair in AdminBoard
3. Optimized imports

=======================================================================================================================

v0.5.1b  --  17.09.2018

1. Optimized signals in accounts (app) -> Separated create and update Researcher methods
2. Fixed changelog.txt

=======================================================================================================================

v0.5b  --  13.09.2018

1. TODO 4: View Portfolio, Balance and PNL for a given strategy for the day / time interval / overall
2. Added pnl-element in list_filter for StrategyPortfolio in AdminBoard
3. Updated README.md

=======================================================================================================================

v0.4.3b  --  13.09.2018

1. Fixed models in trading (app)
2. Fixed Researcher model in accounts (app)
3. Fixed some names

=======================================================================================================================

v0.4.2b  --  12.09.2018

1. Fixed Researcher model in accounts (app)
2. Fixed signals.py
3. Fixed __init__.py -> added default config for accounts (app)
4. Fixed README.md

=======================================================================================================================

v0.4.1b  --  11.09.2018

1. Fixed relationship of models in accounts (app)
2. Added verbose_name_plural in models in trading (app)
3. Fixed __str__ of models in trading (app) & accounts (app)
4. Added filedsets in AdminBoard
5. Added _log_cleaner.py for delete 'recent actions' in AdminBoard
6. Fixed some names

=======================================================================================================================

v0.4b  --  11.09.2018

1. TODO 1: View Strategies with statuses (active, inactive), the possibility of switching the status of the Stratigies
2. TODO 2: View Positions for a given Strategy for the day / time interval / all
3. TODO 3: View Trades for a given Strategy for the day / time interval / all
4. Fixed models in trading (app)
5. Optimized relationships of models in trading (app)
6. Fixed filters in AdminBoard
7. Updated README.md

=======================================================================================================================

v0.3.2b  --  11.09.2018

1. Optimized imports
2. Fixed docstrings
3. Updated README.md
4. Use TZ = False
5. Updated requirements.txt

=======================================================================================================================

v0.3.1b  --  06.09.2018

1. Fixed name of project and database

=======================================================================================================================

v0.3b  --  02.09.2018

1. Fixed Researcher model (implemented through One-to-One field with Django-User-model)
2. Added signals.py and some params in accounts (app)
3  Fixed relationships of models in trading (app)
4. Fixed model names - added verbose_name_plural in trading (app)

=======================================================================================================================

v0.2b  --  31.08.2018

1. Added django-model-utils (lib) for choice-fields (period, execution_type, trading_type)
2. Added models in trading (app): BaseTicker, QuoteTicker, Exchange, Pair, Strategy, StrategyPortfolio, Position, Trade
3. Registered models in AdminBoard: Strategy, StrategyPortfolio, Position, Trade
4. Added Researcher model (rewritten Django-User-model)
5. Added auth_user_model in settings.py
6. Added docstrings
7. Added _logger.py
8. Fixed urls.py
9. Fixed some bugs in settings.py
10. Added TODO-markers (admin.py)
11. Updated requirements.txt
12. Added layout of README.md

=======================================================================================================================

v0.1b  --  30.08.2018

0. Initial commit.

#######################################################################################################################