from django.contrib.admin import ModelAdmin, register

from trading.models import (BaseTicker, QuoteTicker, Exchange, Pair, Strategy,
                            StrategyPortfolio, Position, Trade)
from utils.utils import is_member


@register(BaseTicker)
class BaseTickerAdmin(ModelAdmin):

    list_display = [field.name for field in BaseTicker._meta.fields]

    class Meta:
        model = BaseTicker


@register(QuoteTicker)
class QuoteTickerAdmin(ModelAdmin):

    list_display = [field.name for field in QuoteTicker._meta.fields]

    class Meta:
        model = QuoteTicker


@register(Exchange)
class ExchangeAdmin(ModelAdmin):

    list_display = [field.name for field in Exchange._meta.fields]

    class Meta:
        model = Exchange


@register(Pair)
class PairAdmin(ModelAdmin):

    list_display = [field.name for field in Pair._meta.fields]

    class Meta:
        model = Pair


@register(Strategy)
class StrategyAdmin(ModelAdmin):

    list_display = [field.name for field in Strategy._meta.fields]

    list_filter = ['created_at']

    list_editable = ['running']

    def get_readonly_fields(self, request, obj=None):
        if is_member(request, 'Researchers'):
            return ['created_at',
                    'strategy',
                    'params',
                    'period',
                    'comment']
        return []

    class Meta:
        model = Strategy


@register(StrategyPortfolio)
class StrategyPortfolioAdmin(ModelAdmin):

    list_display = ['created_at',
                    'strategy',
                    'portfolio',
                    'base_ticker',
                    'balance',
                    'pnl']

    list_filter = ['created_at',
                   'strategy']

    def get_readonly_fields(self, request, obj=None):
        if is_member(request, 'Researchers'):
            return self.readonly_fields
        return []

    class Meta:
        model = StrategyPortfolio


@register(Position)
class PositionAdmin(ModelAdmin):

    list_display = [field.name for field in Position._meta.fields]

    list_filter = ['created_at',
                   'strategy']

    def get_readonly_fields(self, request, obj=None):
        if is_member(request, 'Researchers'):
            return self.readonly_fields
        return []

    class Meta:
        model = Position


@register(Trade)
class TradeAdmin(ModelAdmin):

    list_display = [field.name for field in Trade._meta.fields]

    list_filter = ['created_at',
                   'strategy']

    def get_readonly_fields(self, request, obj=None):
        if is_member(request, 'Researchers'):
            return self.readonly_fields
        return []

    class Meta:
        model = Trade
