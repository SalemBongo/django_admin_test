from django.db import models
from django.contrib.postgres.fields import JSONField
from model_utils import Choices


class BaseTicker(models.Model):
    """Model of Base ticker."""

    base_ticker = models.CharField(max_length=120,
                                   blank=False,
                                   unique=True)

    verbose_name = 'Base Ticker'
    verbose_name_plural = 'Base Tickers'

    def __str__(self):
        return self.base_ticker


class QuoteTicker(models.Model):
    """Model of Quote ticker."""

    quote_ticker = models.CharField(max_length=120,
                                    blank=False,
                                    unique=True)

    verbose_name = 'Quote Ticker'
    verbose_name_plural = 'Quote Tickers'

    def __str__(self):
        return self.quote_ticker


class Exchange(models.Model):
    """Model of currency Exchange."""

    exchange = models.CharField(max_length=120,
                                blank=False,
                                unique=True)

    def __str__(self):
        return self.exchange


class Pair(models.Model):
    """Model of tickers Pair."""

    base_ticker = models.ForeignKey(BaseTicker,
                                    on_delete=models.SET_NULL,
                                    null=True)
    quote_ticker = models.ForeignKey(QuoteTicker,
                                     on_delete=models.SET_NULL,
                                     null=True)
    exchange = models.ForeignKey(Exchange,
                                 on_delete=models.SET_NULL,
                                 null=True)

    def __str__(self):
        return 'Pair: {0} - {1} | Exchange: {2}'.format(
            self.base_ticker, self.quote_ticker, self.exchange)


class Strategy(models.Model):
    """Model of trading Strategy."""

    created_at = models.DateTimeField(auto_now_add=True,
                                      auto_now=False)
    strategy = models.CharField(max_length=120,
                                blank=False,
                                unique=True)
    params = JSONField(blank=False,
                       unique=True)
    period = models.CharField(max_length=120,
                              choices=Choices('daily',
                                              'hourly'),
                              blank=False)
    running = models.BooleanField(blank=False,
                                  default=True)
    comment = models.TextField(null=True,
                               blank=True)

    class Meta:
        verbose_name = 'Strategy'
        verbose_name_plural = 'Strategies'

    def __str__(self):
        return 'Strategy: {0} | Active: {1}'.format(
            self.strategy, self.running)


class StrategyPortfolio(models.Model):
    """Model of trading Strategy Portfolio with PNL calculation."""

    created_at = models.DateTimeField(auto_now_add=True,
                                      auto_now=False)
    strategy = models.ForeignKey(Strategy,
                                 on_delete=models.SET_NULL,
                                 null=True)
    base_ticker = models.ForeignKey(BaseTicker,
                                    on_delete=models.SET_NULL,
                                    null=True)
    balance = models.DecimalField(max_digits=40,
                                  decimal_places=20,
                                  null=True)
    portfolio = JSONField(blank=False,
                          unique=True)

    class Meta:
        verbose_name = 'Strategy Portfolio'
        verbose_name_plural = 'Strategy Portfolios'

    def pnl(self):
        return self.balance - StrategyPortfolio.objects.first().balance

    def __str__(self):
        return 'Strategy: {0} | Portfolio: {1}'.format(
            self.strategy, self.portfolio)


class Position(models.Model):
    """Model of trading Position."""

    created_at = models.DateTimeField(auto_now_add=True,
                                      auto_now=False)
    quote_ticker = models.ForeignKey(QuoteTicker,
                                     on_delete=models.SET_NULL,
                                     null=True)
    position = models.DecimalField(max_digits=40,
                                   decimal_places=20,
                                   null=False)
    strategy = models.ForeignKey(Strategy,
                                 on_delete=models.SET_NULL,
                                 null=True)

    def __str__(self):
        return 'Quote: {0} | Position: {1} | Strategy: {2}'.format(
            self.quote_ticker, self.position, self.strategy)


class Trade(models.Model):
    """Model of Trade."""

    created_at = models.DateTimeField(auto_now_add=True,
                                      auto_now=False)
    quote_volume = models.DecimalField(max_digits=40,
                                       decimal_places=20,
                                       null=False)
    base_volume = models.DecimalField(max_digits=40,
                                      decimal_places=20,
                                      null=False)
    price = models.DecimalField(max_digits=40,
                                decimal_places=20,
                                null=False)
    execution_type = models.CharField(max_length=120,
                                      choices=Choices('naive',
                                                      'uniform',
                                                      'vwap'),
                                      blank=False)
    trading_type = models.CharField(max_length=120,
                                    choices=Choices('paper',
                                                    'live'),
                                    blank=False)
    pair = models.ForeignKey(Pair,
                             on_delete=models.SET_NULL,
                             null=True)
    position = models.ForeignKey(Position,
                                 on_delete=models.SET_NULL,
                                 null=True)
    strategy = models.ForeignKey(Strategy,
                                 on_delete=models.SET_NULL,
                                 null=True)

    def __str__(self):
        return 'Pair: {0} | Position: {1} | Strategy: {2}'.format(
            self.pair, self.position, self.strategy)
