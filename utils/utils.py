import logging as log
from threading import Thread


def threaded(func):
    """Thread-decorator."""
    def wrapper(*args, **kwargs):
        my_thread = Thread(target=func,
                           args=args,
                           kwargs=kwargs)
        my_thread.start()
        return my_thread
    return wrapper


def is_member(request, group):
    """Check user membership."""
    return request.user.groups.filter(name=group).exists()


"""Config for logger."""
log.basicConfig(filename='_log.txt',
                level=log.DEBUG,
                format=u'%(filename)s  [LINE: %(lineno)d]# '
                       u' %(levelname)s  [%(asctime)s]  %(message)s')
