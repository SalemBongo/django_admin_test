django_admin_test
---

START:

1. pip install -r requirements.txt

2. Create usergroup "Researchers" with permissions for models (in AdminBoard):
  * Strategy --> view, change
  * Position --> view
  * Trades --> view
  * StrategyPortfolio --> view


TODOs [IMPLEMENTED]:
1. View Strategies with statuses (active, inactive), the possibility of switching the status of the strategies.
2. View Positions for a given strategy for the day / time interval / all.
3. View Trades for a given strategy for the day / time interval / all.
4. View Portfolio, Balance and PNL for a given strategy for the day / time interval / overall.
