from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from trading.models import Strategy
from utils.utils import threaded


class Researcher(models.Model):
    """
    Model of Researcher, implemented through default Django-model of User.
    Signals for create and update Researcher. Run in threads.
    """

    user = models.OneToOneField(User,
                                on_delete=models.CASCADE)
    strategy = models.ManyToManyField(Strategy,
                                      blank=True)

    @threaded
    @receiver(post_save,
              sender=User,
              dispatch_uid='researcher_create_signal')
    def create_user_researcher(sender, instance, created, **kwargs):
        if created:
            Researcher.objects.create(user=instance)

    @threaded
    @receiver(post_save,
              sender=User,
              dispatch_uid='researcher_update_signal')
    def save_user_researcher(sender, instance, **kwargs):
        instance.researcher.save()

    def __str__(self):
        return 'Researcher: {0} | Strategy: {1}'.format(
            self.user, self.strategy)
