from django.contrib.admin import ModelAdmin, register

from accounts.models import Researcher


@register(Researcher)
class ResearcherAdmin(ModelAdmin):
    list_display = [field.name for field in Researcher._meta.fields]

    class Meta:
        model = Researcher
